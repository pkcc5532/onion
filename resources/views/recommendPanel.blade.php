@extends('layouts.basic')

@section('content')
<div class="container">


    @auth
        @if(Auth::user()->isClient())


        @else
            <div class="row text-center" style="color: #FFFFFF; margin-bottom: 10px;">
                Hi, {{ Auth::user()->name }}
            </div>

        @endif
    @endauth

    @guest

    @endguest



    <div class="container-fluid">
        <div  style="background:#ffffff; border :1px solid #444444;">메이크업 추천 해준다. </div>


        <div class="panel" style="height:500px;">
         추천 메이크업 검색
        </div>
    </div>

</div>
@endsection

@section('scripts')
@include('js.config')
<script src="{{ mix('js/site.js') }}"></script>
@endsection