@extends('layouts.basic')

@section('content')
<div class="container">

    @auth
        @if(Auth::user()->isClient())


        @else
            <div class="row text-center" style="color: #FFFFFF; margin-bottom: 10px;">
                Hi, {{ Auth::user()->name }}
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <a href="{{ route('home.index') }}" class="btn btn-primary">Go to Admin Panel</a>
                </div>
            </div>    
        @endif
    @endauth

    @guest

    @endguest






    <div class="container-fluid">
        <div  style="background:#ffffff; border :1px solid #444444;"> 상품 리스트 </div>

        <div class="panel" style="height:500px;">
    @foreach($products as $product)
                <div class="col-md-3" style="background:#ffffff; text-align:center;">

                <h4>{{$product->brand}} {{$product->name}}</h4>
                    <img src ="{{$product->one_image}}" style="width:150px;"/>
                </div>

            @endforeach

            </div>
        </div>

</div>
@endsection

@section('scripts')
@include('js.config')
<script src="{{ mix('js/site.js') }}"></script>
@endsection