@extends('layouts.basic')

@section('content')
<div class="container">
            
    @auth
        @if(Auth::user()->isClient())
            <book-add-appointments></book-add-appointments>
        @else
            <div class="row text-center" style="color: #FFFFFF; margin-bottom: 10px;">
                Hi, {{ Auth::user()->name }}
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <a href="{{ route('home.index') }}" class="btn btn-primary">Go to Admin Panel</a>
                </div>
            </div>    
        @endif
    @endauth

    @guest


    <div class="row text-center">
        <div class="col-sm-12">
            <a href="{{ route('login') }}" class="btn btn-primary" style="margin-top:10px;">브랜드 제품 검색</a>
            <a href="{{ route('login') }}" class="btn btn-primary" style="margin-top:10px;">메이크업 예약하기</a>

        </div>

    </div>
    <div class ="row text-center">
    <div class="col-md-8" style ="padding-top:20px; ">
        <div class="embed-youtube" >
            <iframe  src="https://www.youtube.com/embed/HyJJRQ5SGQo" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
        </div>
    </div>
    </div>


    @endguest

</div>
@endsection

@section('scripts')
@include('js.config')
<script src="{{ mix('js/site.js') }}"></script>
@endsection