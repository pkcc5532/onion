@extends('layouts.basic')

@section('content')
<div class="container">
            
    @auth
        @if(Auth::user()->isClient())

            <div>hello client</div>

        @else
            <div class="row text-center" style="color: #FFFFFF; margin-bottom: 10px;">
                Hi, {{ Auth::user()->name }}
            </div>
            <div class="row text-center">
                <div class="col-sm-12">
                    <a href="{{ route('home.index') }}" class="btn btn-primary">Go to Admin Panel</a>
                </div>
            </div>    
        @endif
    @endauth

    @guest
    <div class="row text-center">
        <div class="col-sm-12">
            <a href="{{ route('login') }}" class="btn btn-primary" style="margin-top:10px;">취향 분석을 위해 로그인 해주세요.!</a>

        </div>

    </div>
    @endguest



    <div class="container-fluid">
        <div class="panel">
            <div class="panel-heading">
                <h3>Products</h3>
                <br>
                <div class="row">
                    <div class="col-sm-2">
                        @can('create-provider', LaraBooking\Models\User::class)
                            <a href="{{ route('home.providers.create') }}" class="btn btn-primary"> <i class="fa fa-plus"></i> Provider</a>
                        @endcan
                    </div>
                    <div class="col-sm-10">
                        <form action="">
                            <div class="input-group">
                                <input type="text" id="search" name="search" class="form-control" placeholder="Search..." value="">
                                <span class="input-group-btn">
                            <button class="btn btn-default" type="submit">Search</button>
                          </span>
                            </div>
                        </form>
                    </div>
                </div>

                <br>

            </div>


            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-stripped table-hover">
                        <thead>
                        <tr>
                            <th>브랜드</th>
                            <th>이름</th>
                            <th class="visible-md visible-lg">설명</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>  <img src ="{{$product->one_image}}" style="width:100px;"/> </td>
                                <td style="width:100px;"><a href="{{ route('home.providers.show', $product->id) }}" >{{ $product->brand }} {{ $product->name }}</a></td>
                                <td class="visible-md visible-lg">{{ $product->desc }}</td>


                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

                {!! $products->render() !!}
            </div>




        </div>






    </div>




    <div>
        <div class="container" >
    @foreach($products as $product)
                <div class="col-md-3" style="background:#ffffff; text-align:center;">

                <h4>{{$product->brand}} {{$product->name}}</h4>
                    <img src ="{{$product->one_image}}" style="width:150px;"/>
                </div>

            @endforeach

            </div>
        </div>


    </div>
</div>
@endsection

@section('scripts')
@include('js.config')
<script src="{{ mix('js/site.js') }}"></script>
@endsection