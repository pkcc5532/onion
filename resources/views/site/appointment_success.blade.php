@extends('layouts.basic')

@section('content')
<div class="container text-center" style="color: white">
    <h3>메이크업 예약이 완료 되었습니다.</h3>
    <span>취소를 원하실 경우, 예약일 이전에 취소 부탁드립니다. </span>
</div>
@endsection