<?php

namespace LaraBooking\Http\Controllers\Site;

use LaraBooking\Models\Product;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use LaraBooking\Http\Controllers\Controller;

class SiteController extends Controller
{


    public function recommendPanel(Request $request){
        $products = Product::where('brand','샤넬' )->paginate(10);
        return view('recommendPanel')
            ->with('products', $products);
    }


    public function productPanel(Request $request){
        $products = Product::where('brand','샤넬' )->paginate(10);
        return view('productPanel')
            ->with('products', $products);
    }


    public function searchPanel(Request $request){
    $products = Product::where('brand','샤넬' )->paginate(10);
    return view('searchPanel')
        ->with('products', $products);
}


    public function reservePanel(Request $request){
       // $products = Product::where('brand','샤넬' )->paginate(10);

        return view('site');

    }



    /**
     * Display the site index.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('site');
    }

    /**
     * Display the appointment success screen.
     *
     * @return \Illuminate\Http\Response
     */
    public function appointmentSuccess(Request $request)
    {
        return view('site.appointment_success');
    }

}